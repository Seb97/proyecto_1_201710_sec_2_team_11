package model.logic;

import model.data_structures.NodoListaD;

public class MergeLista {
	public NodoListaD darMitad(NodoListaD<Comparable> n){
		NodoListaD<Comparable> ahead;
		NodoListaD<Comparable> back;

		if(n==null || n.darSiguiente()==null){
			return n;
		}
		else{
			back=n;
			ahead=back.darSiguiente();
			while(ahead!=null){
				ahead=ahead.darSiguiente();
				if(ahead!=null){
					ahead=ahead.darSiguiente();
					back=back.darSiguiente();
				}
			}
			return back;
		}

	}

	public NodoListaD<Comparable> merge(NodoListaD<Comparable> a, NodoListaD<Comparable> b){
		NodoListaD<Comparable> res=null;
		if(a==null) return b;
		if(b==null) return a;
		if(a.darElemento().compareTo(b.darElemento())<=0){
			res=a;
			res.cambiarSiguiente(merge(a.darSiguiente(),b));
		}
		else{
			res=b;
			res.cambiarSiguiente(merge(b.darSiguiente(),a));
		}
		return res;

	}

	public void sort(NodoListaD<Comparable> cabeza){
		NodoListaD<Comparable> h=null;
		NodoListaD<Comparable> mid=null;
		NodoListaD<Comparable> b=null;
		if(cabeza==null||cabeza.darSiguiente()==null){
			return;
		}
		mid=darMitad(cabeza);
		h=cabeza;
		b=mid.darSiguiente();
		mid.cambiarSiguiente(null);
		sort(h);
		sort(b);
		cabeza=merge(h,b);
	}



}
