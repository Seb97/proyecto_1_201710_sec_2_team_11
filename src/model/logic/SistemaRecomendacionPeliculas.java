package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.Iterator;

import com.sun.scenario.effect.Merge;

import api.ISistemaRecomendacionPeliculas;
import model.vo.*;
import model.data_structures.*;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas
{

	//Atributos
	private ILista<VOPelicula> listaPeliculas;
	private ILista<VOGeneroPelicula> listaPorGeneros;
	private ILista<VOUsuario> usuarios;
	private ILista<VORating> raitings;
	private ILista<VOTag> tags;



	//Metodos

	/**
	 * Carga una lista de peliculas del archivo rutaPeliculas<br>.
	 * @param rutaPeliculas ruta del archivo con listado de peliculas en formato csv.
	 * @return true si se pudo leer el archivo.
	 */
	public boolean cargarPeliculasSR(String rutaPeliculas)
	{
		boolean retorno = false;
		try
		{
			BufferedReader lector = new BufferedReader(new InputStreamReader(new FileInputStream(rutaPeliculas))) ;
			try 
			{
				String lineActual = lector.readLine() ;
				retorno = true;
				while(lineActual != null)
				{
					VOPelicula peliAgregar = null;
					if(lineActual.equals("movieId,title,genres"))
					{
						lineActual = lector.readLine();
					}


					//Se agrega el id de la pelicula.
					String[] datos = lineActual.split(",");
					peliAgregar.setIdPelicula(Long.parseLong(datos[0]));


					//Aqui se agrega la lista de generos.
					ILista<String> generos = null;
					String[] generosAsociados = datos[datos.length -1].split("|");
					for(int i=0 ; i< generosAsociados.length; i++)
					{
						generos.agregarElementoFinal(generosAsociados[i]);
					}
					peliAgregar.setGenerosAsociados(generos);


					//Se agruegan el agno de publicacion.
					String[] ultSinAgno = datos[datos.length - 2].split("(");
					String finalCadena = "";
					for(int j = 0; j < ultSinAgno.length - 1 ; j++ )
					{
						if(finalCadena.equals(""))
						{
							finalCadena = ultSinAgno[j];
						}
						else
						{
							finalCadena = finalCadena + "(" + ultSinAgno[j] ;
						}
					}
					int agnoPublicacion = Integer.parseInt(ultSinAgno[ultSinAgno.length - 1].replace(")",""));
					peliAgregar.setAgnoPublicacion(agnoPublicacion);


					// Se agrega el titulo de la pelicula.
					String titulo = "";
					for(int k = 1 ; k < datos.length - 2 ; k++)
					{
						if(titulo.equals(""))
						{
							titulo = datos[k];
						}
						else
						{
							titulo = titulo + ", " + datos[k];
						}
					}
					titulo = titulo + finalCadena;
					peliAgregar.setTitulo(titulo);


					//Se agregan como nulos o 0 los datos que no se encuentran en el archivo csv
					peliAgregar.setTagsAsociados(null);
					peliAgregar.setNumeroRatings(0);
					peliAgregar.setPromedioRatings(0);
					peliAgregar.setTagsAsociados(null);
					peliAgregar.setNumeroTags(0);

					//Se agrega la pelicula a la lista de peliculas.
					listaPeliculas.agregarElementoFinal(peliAgregar);
					lineActual = lector.readLine();
				}
				lector.close();
			}
			catch (IOException e) 
			{
				return retorno;
			}
		} 
		catch (FileNotFoundException e)
		{
			return retorno;
		}
		return retorno;
	}

	/**
	 * Carga el historial de ratings del archivo rutaRatings<br>
	 * @param rutaRatings ruta del archivo con el historial de ratings en formato csv.
	 * @return true si se pudo leer el archivo.
	 */
	public boolean cargarRatingsSR(String rutaRatings) 
	{
		boolean retorno = false;

		BufferedReader lector;
		try 
		{
			lector = new BufferedReader(new InputStreamReader(new FileInputStream(rutaRatings)));
			try
			{
				String lineActual = lector.readLine();
				retorno = true;
				VORating rateAdd = null;
				if(lineActual.equals("userId, movieId, rating, timestamp"))
				{
					lineActual = lector.readLine();
				}
				while( lineActual != null)
				{
					String[] datos = lineActual.split(",");
					rateAdd.setIdUsuario(Integer.parseInt(datos[0]));
					rateAdd.setIdPelicula(Long.parseLong(datos[1]));
					rateAdd.setRating(Double.parseDouble(datos[2]));
					raitings.agregarElementoFinal(rateAdd);
					lineActual = lector.readLine();
				}
				lector.close();
			}
			catch (IOException e) 
			{
				return retorno;
			}
		} 
		catch (FileNotFoundException e) 
		{

			return retorno;
		}


		return retorno;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) 
	{
		boolean  retorno = false;

		BufferedReader lector;

		try 
		{
			lector = new BufferedReader(new InputStreamReader(new FileInputStream(rutaTags)));

			String lineActual;
			try {
				lineActual = lector.readLine();
				retorno = true;
				if(lineActual.equals("userId, movieId, tag, timestamp"))
				{
					lineActual = lector.readLine();
				}
				while( lineActual !=null )
				{
					boolean ward = false;
					String[] datos = lineActual.split(",");

					Iterator<VOGeneroPelicula> iTGenPel = listaPorGeneros.iterator();
					int posA = 0;
					while(iTGenPel.hasNext() && !ward)
					{
						VOGeneroPelicula aux = iTGenPel.next();
						Iterator<VOPelicula> auxPel = aux.getPeliculas().iterator();
						int pos  = 0;
						while(auxPel.hasNext() && !ward)
						{
							VOPelicula auxAdd = auxPel.next();
							if(Long.parseLong(datos[1]) == auxAdd.getIdPelicula())
							{
								ward = true;
								listaPorGeneros.darElemento(posA).getPeliculas().darElemento(pos).getTagsAsociados().agregarElementoFinal(datos[2]);
							}
							pos ++;
						}
						posA ++;
					}
				}
			} 

			catch (IOException e)
			{
				return retorno;
			}

		}
		catch (FileNotFoundException e)
		{
			return retorno;
		}
		return retorno;
	}

	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return listaPeliculas.darNumeroElementos();

	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return usuarios.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return tags.darNumeroElementos();
	}

	/**
	 * Retorna las n peliculas mas populares por cada uno de los generos 
	 * @param n numero de peliculas populares por genero a retornar
	 * @return Lista donde cada posicion tiene un genero y asociado a este una  lista de peliculas populares
	 */
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n)
	{
		Iterator<VOPelicula> viejalista = listaPeliculas.iterator();
		while( viejalista.hasNext())
		{
			VOPelicula auxiliar = viejalista.next();
			ILista<String> generosAuxiliar = auxiliar.getGenerosAsociados();
			Iterator<String> iteradorGeneros = generosAuxiliar.iterator(); 
			while(iteradorGeneros.hasNext())
			{
				String nombre = iteradorGeneros.next();
				//Caso en que la lista este vacia.
				if( listaPorGeneros.darNumeroElementos() == 0)
				{
					VOGeneroPelicula generoAgregar = null;
					generoAgregar.setGenero(nombre);
					ILista<VOPelicula> peliGen = null;
					peliGen.agregarElementoFinal(auxiliar);
					generoAgregar.setPeliculas(peliGen);
					listaPorGeneros.agregarElementoFinal(generoAgregar);
				}


				//Caso en que la lista tiene el genero registrado.
				else
				{

					boolean ward = false ; 
					Iterator<VOGeneroPelicula> iteradorRetorno = listaPorGeneros.iterator();
					while(iteradorRetorno.hasNext() && !ward)
					{
						VOGeneroPelicula auxiliarGen =  iteradorRetorno.next();
						if(auxiliarGen.getGenero().equals(nombre))
						{
							auxiliarGen.getPeliculas().agregarElementoFinal(auxiliar);
							ward = true;
						} 
					}

					//Caso en que la lista no tenga aun el genero registrado.
					if(ward = false)
					{
						VOGeneroPelicula generoAgregar = null;
						generoAgregar.setGenero(nombre);
						ILista<VOPelicula> peliGen = null;
						peliGen.agregarElementoFinal(auxiliar);
						generoAgregar.setPeliculas(peliGen);
						listaPorGeneros.agregarElementoFinal(generoAgregar);
					}
				}
			}
		}
		ILista<VOGeneroPelicula> listaRetorno = null;
		Iterator<VORating> itRates = raitings.iterator();
		while(itRates.hasNext())
		{
			VORating thisRate = itRates.next();
			Iterator<VOGeneroPelicula> genList = listaPorGeneros.iterator();
			while(genList.hasNext() )
			{
				boolean ward = false;
				ILista<VOPelicula> listPeliAux = genList.next().getPeliculas();
				Iterator<VOPelicula> IterpeliAux = listPeliAux.iterator();
				int pos = 0;
				while(IterpeliAux.hasNext() && !ward)
				{
					VOPelicula peliAux = IterpeliAux.next();
					if(peliAux.getIdPelicula() == thisRate.getIdPelicula())
					{
						double nuevoProm = (peliAux.getPromedioRatings() * peliAux.getNumeroRatings() + thisRate.getRating())/ (peliAux.getNumeroRatings() + 1);
						listPeliAux.darElemento(pos).setPromedioRatings(nuevoProm);
						listPeliAux.darElemento(pos).setNumeroRatings(peliAux.getNumeroRatings() + 1);
						ward = true;
					}
					pos ++;
				}
			}
		}


		//Se ordenan por rating las pelis de cada genero para despues sacar las n pelis con mejor rating y finalmente agregarlas en la lista retorno.

		MergeLista merger = new MergeLista();
		for(int i = 0 ; i <listaPorGeneros.darNumeroElementos() ; i++)
		{
			merger.sort(listaPorGeneros.darElemento(i).getPeliculas().darPrimer());
			VOGeneroPelicula agregando = new VOGeneroPelicula();
			agregando.setGenero(listaPorGeneros.darElemento(i).getGenero());
			ILista<VOPelicula> listadding = null;
			for (int j = 1 ; j < n + 1 ; j++)
			{
				listadding.agregarElementoFinal(listaPorGeneros.darElemento(i).getPeliculas().darElemento(listaPorGeneros.darElemento(i).getPeliculas().darNumeroElementos() -j));
			}
			agregando.setPeliculas(listadding);
			listaRetorno.agregarElementoFinal(agregando);
		}
		return listaRetorno;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR()
	{
		MergeLista merger = new MergeLista();
		merger.sort(listaPeliculas.darPrimer());
		return listaPeliculas;
	}
	public ILista<VOPelicula> mejorPorRating()
	{
		ILista<VOPelicula> mejores = null;
		ILista<VOGeneroPelicula>  auxGen = listaPorGeneros;
		MergeLista merger = new MergeLista();
		for ( int i = 0; i < auxGen.darNumeroElementos() ; i ++)
		{
			ILista<VOPelicula> auxPeli = auxGen.darElemento(i).getPeliculas();
			for( int j = 0; j< auxPeli.darNumeroElementos() ; j ++)
			{
				auxPeli.darElemento(j).setAgnoPublicacion(0);
			}
			merger.sort(auxPeli.darPrimer());
			mejores.agregarElementoFinal(auxPeli.darElemento(auxPeli.darNumeroElementos() -1 ));
		}
		return mejores;
	}


	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() 
	{
		//La listaPorgeneros contiene ya todos las pelis con sus tags respectivos, por eso lo unico necesario es hacer el merge si es que no se ha realizado anteriormente.
		ILista<VOGeneroPelicula> reList = listaPorGeneros;
		MergeLista merger = new  MergeLista();
		for(int i = 0 ; i < reList.darNumeroElementos() ; i++)
		{
			merger.sort(reList.darElemento(i).getPeliculas().darPrimer());
		}
		return reList;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		Stack<VOPelicula> zz = new Stack<>();
		Stack<VOPeliculaPelicula> vv= new Stack<>();
		try {
			BufferedReader lector = new BufferedReader(new InputStreamReader(new FileInputStream(rutaRecomendacion)));
			int j = n;
			String a = lector.readLine();
			while(a!=null&&j>0){
				a=lector.readLine();
				j--;
				String[] c = a.split(",");
				VOPelicula z = new VOPelicula();
				z.setIdPelicula(Integer.parseInt(c[0]));
				z.setNumeroRatings(Integer.parseInt(c[1]));
				zz.agregarElementoFinal(z);

			}
			lector.close();
			Stack<VOPelicula> guarda=zz;
			Iterator oter = listaPeliculas.iterator();

			while(oter.hasNext()){
				NodoListaD v = (NodoListaD) oter.next();
				VOPelicula culia = (VOPelicula) v.darElemento();
				VOPeliculaPelicula avila = new VOPeliculaPelicula();
				ILista<VOPelicula> culia2 = new Stack();
				zz=guarda;
				VOPelicula temp = zz.pop();
				zz.push(temp);
				zz.push(temp);
				while(zz.pop()!=null){
					VOPelicula temp2 = zz.pop();
					if(temp.getPromedioRatings()-temp2.getPromedioRatings()<=0.5||temp.getPromedioRatings()-temp2.getPromedioRatings()>=-0.5){
						avila.setPelicula(culia);
						culia2.agregarElementoFinal(temp2);
						avila.setPeliculasRelacionadas(culia2);
					}
					vv.agregarElementoFinal(avila);
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return vv;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula)
	{
	ILista<VORating> listre= null;
	Iterator<VORating> iTRate = raitings.iterator();
	while(iTRate.hasNext())
	{
		VORating adding = iTRate.next();
		if(adding.getIdPelicula() == idPelicula)
		{
			listre.agregarElementoFinal(adding);
		}
	}
	return listre;
	
	}

	public ILista<VOGeneroUsuario> usuariosActivosSR(int n){

		Iterator z = listaPeliculas.iterator();
		ILista<VOGeneroTag> listaOp= new ListaDobleEncadenada<>();
		ILista<VOGeneroUsuario> rta = new ListaDobleEncadenada<>();
		int ch=n;
		while(z.hasNext()){
			NodoListaD po = (NodoListaD) z.next();
			VOGeneroTag a = new VOGeneroTag();
			VOPelicula peli =(VOPelicula)po.darElemento();
			ILista<String> VOG = peli.getGenerosAsociados();
			Iterator<String> y = VOG.iterator();
			while(y.hasNext()){
				String avila =y.next();
				Iterator zz = tags.iterator();
				while(zz.hasNext()){
					NodoListaD avila1 = (NodoListaD) zz.next();
					VOTag pup = (VOTag) avila1.darElemento();
					String ak = pup.getTag();
					String[] sus = ak.split(",");
					long av = Long.parseLong(sus[1]);
					if(peli.getIdPelicula()==av){
						a.setGenero(avila);
						a.getTags().agregarElementoFinal(ak);
					}

				}
				listaOp.agregarElementoFinal(a);
			}
		}
		Iterator itero = listaOp.iterator();
		while(itero.hasNext()&&ch>0){
			NodoListaD pup = (NodoListaD) itero.next();
			VOGeneroTag imp = (VOGeneroTag) pup.darElemento();
			VOGeneroUsuario s = new VOGeneroUsuario();
			s.setGenero(imp.getGenero());
			ILista<String> v = imp.getTags();
			Iterator j = v.iterator();
			Iterator i = v.iterator();
			VOUsuarioConteo avila = new VOUsuarioConteo();
			VOUsuarioConteo avila1 = new VOUsuarioConteo();
			while(j.hasNext()){
				VOUsuarioConteo aviltemp = new VOUsuarioConteo();
				NodoListaD pip = (NodoListaD) j.next();
				String ont = (String) pip.darElemento();
				String[] kon = ont.split(",");
				while(i.hasNext()){
					NodoListaD pip1 = (NodoListaD) i.next();
					String ont1 = (String) pip1.darElemento();
					String[] kon1 = ont1.split(",");
					if(Long.parseLong(kon[0])==Long.parseLong(kon1[0])){
						aviltemp.setIdUsuario(Long.parseLong(kon[0]));
						aviltemp.setConteo(aviltemp.getConteo()+1);

						if(avila.getIdUsuario()==0||avila1.getIdUsuario()==0){
							if(avila.getIdUsuario()==0){
								avila=aviltemp;

							}
							else{
								avila1=aviltemp;
							}
						}
						else{
							if(aviltemp.getIdUsuario()>avila.getIdUsuario()){
								avila1 = avila;
								avila=aviltemp;
							}
						}
					}


				}
			}
			ILista<VOUsuarioConteo> avilas = new ListaDobleEncadenada<>();
			avilas.agregarElementoFinal(avila1);
			avilas.agregarElementoFinal(avila);
			s.setUsuarios(avilas);
			rta.agregarElementoFinal(s);
			ch--;
		}
		return rta; 


	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		// TODO Auto-generated method stub
		ILista<VOUsuario> rta = new ListaDobleEncadenada<>();
		Iterator iter = raitings.iterator();
		Iterator iter1 = raitings.iterator();
		while(iter.hasNext()){
			NodoListaD avila1 = (NodoListaD) iter.next();
			ILista<VOUsuario> subRta = new ListaDobleEncadenada<>();
			VOTag pup = (VOTag) avila1.darElemento();
			String ak = pup.getTag();
			String[] sus = ak.split(",");
			VOUsuario temp = new VOUsuario();
			temp.setPrimerTimestamp(Long.parseLong(sus[3]));
			temp.setNumRatings(Integer.parseInt(sus[2]));
			temp.setIdUsuario(Long.parseLong(sus[0]));

			while(iter1.hasNext()){
				NodoListaD avila = (NodoListaD) iter1.next();
				VOTag pup1 = (VOTag) avila.darElemento();
				String ak1 = pup1.getTag();
				String[] sus1 = ak1.split(",");
				VOUsuario temp1 = new VOUsuario();
				temp1.setPrimerTimestamp(Long.parseLong(sus1[3]));
				temp1.setNumRatings(Integer.parseInt(sus1[2]));
				temp1.setIdUsuario(Long.parseLong(sus1[0]));
				if(temp.getIdUsuario()==temp1.getIdUsuario()){
					subRta.agregarElementoFinal(temp1);
				}
			}
			NodoListaD s= subRta.darMayor((ListaDobleEncadenada<VOUsuario>) subRta);
			rta.agregarElementoFinal((VOUsuario) s.darElemento());

		}
		MergeLista ez = new MergeLista();
		ez.sort(rta.darPrimer());
		return rta;
	}

	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		// TODO Auto-generated method stub





	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		// TODO Auto-generated method stub
		Iterator z = listaPeliculas.iterator();
		ILista<VOUsuarioGenero> rta = new ListaDobleEncadenada<>();
		while(z.hasNext()){
			NodoListaD po = (NodoListaD) z.next();
			ILista<VOGeneroTag> listaOp= new ListaDobleEncadenada<>();
			int idUsuariotemp = 0;
			VOGeneroTag a = new VOGeneroTag();
			VOPelicula peli =(VOPelicula)po.darElemento();
			VOUsuarioGenero subRta = new VOUsuarioGenero();
			ILista<String> VOG = peli.getGenerosAsociados();
			Iterator<String> y = VOG.iterator();
			while(y.hasNext()){
				String avila =y.next();
				Iterator zz = tags.iterator();
				Iterator zzz = tags.iterator();
				while(zz.hasNext()){
					NodoListaD avila1 = (NodoListaD) zz.next();
					VOTag pup = (VOTag) avila1.darElemento();
					String ak = pup.getTag();
					String[] sus = ak.split(",");
					long av = Long.parseLong(sus[0]);
					while(zzz.hasNext()){
						NodoListaD avila2 = (NodoListaD) zzz.next();
						VOTag pup1 = (VOTag) avila2.darElemento();
						String ak1 = pup1.getTag();
						String[] sus1 = ak.split(",");
						if(peli.getIdPelicula()==av&&Integer.parseInt(sus[0])==Integer.parseInt(sus1[0])){
							a.setGenero(avila);
							a.getTags().agregarElementoFinal(ak);
							idUsuariotemp=Integer.parseInt(sus[0]);

						}
					}
					listaOp.agregarElementoFinal(a);
					subRta.setListaGeneroTags(listaOp);
					subRta.setIdUsuario(idUsuariotemp);
					rta.agregarElementoFinal(subRta);
				}
			}

		}


		return rta;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub
		ILista<VOTag> rta = new ListaDobleEncadenada<>();
		Iterator zz = tags.iterator();
		while(zz.hasNext()){
			NodoListaD avi = (NodoListaD) zz.next();
			VOTag pup = (VOTag) avi.darElemento();
			String ak = pup.getTag();
			String[] sus = ak.split(",");
			if(Integer.parseInt(sus[0])==idPelicula){
				rta.agregarElementoFinal(pup);
			}
		}
		MergeLista ez = new MergeLista();
		ez.sort(rta.darPrimer());
		return rta;
	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		// TODO Auto-generated method stub

	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR() {
		// TODO Auto-generated method stub
		return null;
	}


}
