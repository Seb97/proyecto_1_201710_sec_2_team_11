package model.vo;

public class VORating implements Comparable {

	private long idUsuario;
	private long idPelicula;
	private double rating;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}

	public int compareTo(Object o) {
		if(this.getRating() == ((VORating) o).getRating())
			return 0;
		else if(this.getRating()> ((VORating) o).getRating())
			return 1;
		else
			return -1;
	}
	


}
