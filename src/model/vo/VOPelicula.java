package model.vo;

import model.data_structures.ILista;

public class VOPelicula implements Comparable{
	
	private long idPelicula; 
	private String titulo;
	private int numeroRatings;
	private int numeroTags;
	private double promedioRatings;
	private int agnoPublicacion;

	private ILista<String> tagsAsociados;
	
	private ILista<String> generosAsociados;
	
	
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idUsuario) {
		this.idPelicula = idUsuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

@Override
public int compareTo(Object o) 
{
	if( this.getAgnoPublicacion() < ((VOPelicula) o).getAgnoPublicacion() )
	return -1;
	else if( this.getAgnoPublicacion() > ((VOPelicula) o).getAgnoPublicacion())
		return 1;
	if(this.getPromedioRatings() > ((VOPelicula) o).getPromedioRatings())
	{
		return 1;
	}
	else if( this.getPromedioRatings() < ((VOPelicula) o).getPromedioRatings())
	return -1;
	else{ 
		return this.getTitulo().compareTo(((VOPelicula) o).getTitulo());
	}
}
	

}
