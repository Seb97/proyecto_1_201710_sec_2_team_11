package model.vo;

import model.data_structures.ILista;

public class VOUsuarioGenero implements Comparable{
	
private long idUsuario;
	
	private ILista<VOGeneroTag> listaGeneroTags;
	
	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public ILista<VOGeneroTag> getListaGeneroTags() {
		return listaGeneroTags;
	}

	public void setListaGeneroTags(ILista<VOGeneroTag> listaGeneroTags) {
		this.listaGeneroTags = listaGeneroTags;
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	
	

}
