package model.vo;

import model.data_structures.ILista;

public class VOPeliculaUsuario implements Comparable{
	
	private long idPelicula;
	private ILista<VOUsuario> usuariosRecomendados;
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public ILista<VOUsuario> getUsuariosRecomendados() {
		return usuariosRecomendados;
	}
	public void setUsuariosRecomendados(ILista<VOUsuario> usuariosRecomendados) {
		this.usuariosRecomendados = usuariosRecomendados;
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
