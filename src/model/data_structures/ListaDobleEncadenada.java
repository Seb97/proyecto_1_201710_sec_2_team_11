package model.data_structures;


import java.util.Iterator;

public class ListaDobleEncadenada<T extends Comparable> implements ILista<T> {
	
	private NodoListaD<T> primer;
	private int cantidadEle;
	private NodoListaD<T> end;
	private NodoListaD<T> actual;
	
	public ListaDobleEncadenada() {
		// TODO Auto-generated constructor stub
		primer = null;
		end=null;
		actual=null;
		cantidadEle=0;
		
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		actual = primer;
		Iterator<T> iterator= new Iterator<T>() 
		{

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return actual.darSiguiente()==null?false:true;
			}

			@Override
			public T next() {
				actual = actual.darSiguiente();
				return (T)actual;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};
		return iterator;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		if(primer==null){
			primer=new NodoListaD<T>(elem,0);
			end= primer;
			actual=primer;
			cantidadEle++;
		}
		else{
			NodoListaD<T> aux = new NodoListaD<T>(elem,cantidadEle+1);
			aux.cambiarAnterior(end);
			end.cambiarSiguiente(aux);
			Iterator a = iterator();
			while(a.hasNext()){
				NodoListaD<T> cosa = (NodoListaD<T>) a.next();
			}
			end = actual;
			cantidadEle++;
		}
		
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		Iterator<T> a = iterator();
		actual = primer;
		if(primer!=null)
		{
			while(a.hasNext()){
				@SuppressWarnings("unchecked")
				NodoListaD<T> aux =(NodoListaD<T>)a.next();
				if(pos==0){
					return actual.darElemento();
				}
			}
		}
		return null;
		
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return cantidadEle;
	}
	



	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		NodoListaD<T> aux;
		if(primer!=null){
			if(pos==0){
				aux=primer;
				primer=primer.darSiguiente();
				cantidadEle--;
				return aux.darElemento();
				
			}
			else if(pos== darNumeroElementos()-1){
				aux=end;
				end = end.darAnterior();
				cantidadEle--;
				return aux.darElemento();
			}
			darElemento(pos);
			aux = actual;
			actual=actual.darAnterior();
			aux.darAnterior().cambiarSiguiente(aux.darSiguiente());
			aux.darSiguiente().cambiarAnterior(actual);
			cantidadEle--;
			return aux.darElemento();
		}
		return null;
	}
	public NodoListaD darNodo(int pos) {
		// TODO Auto-generated method stub
		Iterator<T> a = iterator();
		actual = primer;
		if(primer!=null){
			while(a.hasNext()){
				NodoListaD<T> aux =(NodoListaD)a.next();
				if(pos==0){
					return actual;
				}
			}
		}
		return null;
		
	}
	public void Parit(int pos) {
		// TODO Auto-generated method stub
		NodoListaD<T> aux;
		if(primer!=null){
			if(pos==0){
				aux=primer;
				primer=primer.darSiguiente();
				cantidadEle--;
				
			}
			else if(pos== darNumeroElementos()-1){
				aux=end;
				end = end.darAnterior();
				cantidadEle--;
			}
			darElemento(pos);
			aux = actual;
			actual=actual.darAnterior();
			actual.cambiarSiguiente(null);
			ListaDobleEncadenada<T> a = new ListaDobleEncadenada<>();
			a.agregarElementoFinal(aux.darElemento());
			cantidadEle -= a.darNumeroElementos();
		}

	}

	@Override
	public NodoListaD darMayor(ListaDobleEncadenada<T> temp) {
		// TODO Auto-generated method stub
		int medio = temp.cantidadEle/2;
		ListaDobleEncadenada<T> temp2 = new ListaDobleEncadenada<>();
		temp2.agregarElementoFinal((T) temp.darNodo(medio));
		temp.Parit(medio);
		if(temp.cantidadEle==0||temp2.cantidadEle==0){
			return null;
		}
		else if(temp.primer.darSiguiente()==null&&temp2.primer.darSiguiente()==null){
			if(temp.primer.darElemento().compareTo(temp2.primer.darElemento())<0){
				return temp.primer;
			}
			else{
				return temp2.primer;
			}
		}
		else if(temp.primer == null){
			return temp2.primer;
		}
		else{
			return temp.darMayor(temp2);
		}
		
	}

	@Override
	public void eliminarRepetidos() {
		// TODO Auto-generated method stub
		Iterator<T> a = iterator();
		Iterator<T> c = iterator();
		actual = primer;
		if(primer!=null){
			while(a.hasNext()){
				NodoListaD<T> aux =(NodoListaD)a.next();
				while(a.hasNext()){
					NodoListaD<T> aux1 =(NodoListaD)c.next();
					if(aux.equals(aux1)){
						eliminarElemento(aux1.darIndex());
					}
				}
			}
		}
		
	}
	public NodoListaD darPrimer(){
		return primer;
	}
	

}
