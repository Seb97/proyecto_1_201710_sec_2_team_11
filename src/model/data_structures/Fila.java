package model.data_structures;

public class Fila<T extends Comparable> extends ListaDobleEncadenada<T>{
	public Fila(){
		super();
	}
	public T deQueue(){
		return eliminarElemento(darNumeroElementos()-1);
	}
	public void Queue(T elem){
		super.agregarElementoFinal(elem);
	}

}
