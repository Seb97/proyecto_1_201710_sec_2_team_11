package model.data_structures;

public class NodoListaD<T extends Comparable> {
	
	private NodoListaD<T> anterior;
	
	private NodoListaD<T> siguiente;
	
	private T elemento;
	
	private int index;
	
	public NodoListaD(T elem, int pIndex){
		elemento= elem;
		anterior=null;
		siguiente=null;
		index = pIndex;
	}
	
	public NodoListaD<T> darSiguiente(){
		return siguiente;
	}
	
	public NodoListaD<T> darAnterior(){
		return anterior;
	}
	
	public T darElemento(){
		return elemento;
	}
	public void cambiarSiguiente(NodoListaD<T> pNodo){
		siguiente=pNodo;
	}
	public void cambiarAnterior(NodoListaD<T> pNodo){
		anterior=pNodo;
	}
	public int darIndex(){
		return index;
	}
}
