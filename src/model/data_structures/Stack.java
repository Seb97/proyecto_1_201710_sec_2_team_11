package model.data_structures;

public class Stack <T extends Comparable> extends ListaDobleEncadenada<T>{
	public Stack(){
		super();
	}
	public T pop(){
		return eliminarElemento(0);
	}
	public void push(T elem){
		super.agregarElementoFinal(elem);
	}

}
