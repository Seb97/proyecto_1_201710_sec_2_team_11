package model.data_structures;


public interface ILista<T extends Comparable> extends Iterable<T>{
	
	
	
	public void agregarElementoFinal(T elem);
	
	public T darElemento(int pos);
	
	public T eliminarElemento(int pos);
	
	public int darNumeroElementos();
	
	public NodoListaD darMayor(ListaDobleEncadenada<T> temp);
	
	public void eliminarRepetidos();
	public NodoListaD darPrimer();
	
	

}
